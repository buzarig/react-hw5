import { modalTypes } from "../types";

const initialState = {
  showModal: false,
  modalId: null,
  modalSubmit: null,
};

export const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case modalTypes.MODAL_OPEN:
      return {
        showModal: true,
        modalId: action.payload.modalId,
        modalSubmit: action.payload.modalSubmit,
      };

    case modalTypes.MODAL_CLOSE:
      return { showModal: false, modalId: null, modalSubmit: null };

    default:
      return state;
  }
};
