import React from "react";
import Card from "../card/Card";
import styles from "./Products.module.scss";
import PropTypes from "prop-types";

function Products(props) {
  return (
    <div className={styles.ProductsList}>
      {props.products.map((product) => (
        <Card
          productObj={product}
          favoriteProducts={props.favoriteProducts}
          cartProducts={props.cartProducts}
          imgSrc={product.image}
          title={product.name}
          color={product.color}
          price={product.price}
          key={product.article}
          openModal={props.openModal}
          updateCart={props.updateCart}
          updateFavorite={props.updateFavorite}
          favoriteRemover={props.favoriteRemover}
          cartRemover={props.cartRemover}
        />
      ))}
    </div>
  );
}

Products.propTypes = {
  products: PropTypes.array,
  imgSrc: PropTypes.string,
  title: PropTypes.string,
  color: PropTypes.string,
  price: PropTypes.number,
  openModal: PropTypes.func,
  updateCart: PropTypes.func,
  updateFavorite: PropTypes.func,
};

Products.defaultProps = {
  color: "black",
};

export default Products;
