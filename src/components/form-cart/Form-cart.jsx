import React from "react";
import styles from "./Form-cart.module.scss";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";

const validationSchema = Yup.object().shape({
  firstName: Yup.string().required("Required"),
  lastName: Yup.string().required("Required"),
  age: Yup.number()
    .required("Required")
    .positive("Age must be a positive number"),
  address: Yup.string().required("Required"),
  phone: Yup.string().required("Required"),
});

const FormCart = ({ updateCart }) => {
  return (
    <Formik
      initialValues={{
        firstName: "",
        lastName: "",
        age: "",
        address: "",
        phone: "",
      }}
      validationSchema={validationSchema}
      onSubmit={(values) => {
        console.log("Purchased items:", JSON.parse(localStorage.Cart));
        console.log("User information:", values);
        localStorage.setItem("Cart", JSON.stringify([]));
        updateCart();
      }}
    >
      {({ errors, touched }) => (
        <Form className={styles.Form}>
          <div className={styles.Input}>
            <label className={styles.Label} htmlFor="firstName">
              First Name:
            </label>
            <Field name="firstName" />
            <ErrorMessage name="firstName" />
          </div>

          <div className={styles.Input}>
            <label className={styles.Label} htmlFor="lastName">
              Last Name:
            </label>
            <Field name="lastName" />
            <ErrorMessage name="lastName" />
          </div>

          <div className={styles.Input}>
            <label className={styles.Label} htmlFor="age">
              Age:
            </label>
            <Field name="age" type="number" />
            <ErrorMessage name="age" />
          </div>

          <div className={styles.Input}>
            <label className={styles.Label} htmlFor="address">
              Address:
            </label>
            <Field name="address" />
            <ErrorMessage name="address" />
          </div>

          <div className={styles.Input}>
            <label className={styles.Label} htmlFor="phone">
              Phone:
            </label>
            <Field name="phone" />
            <ErrorMessage name="phone" />
          </div>

          <button type="submit">Checkout</button>
        </Form>
      )}
    </Formik>
  );
};

export default FormCart;
